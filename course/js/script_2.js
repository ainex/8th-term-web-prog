window.onload = function () {
    var id_images = ['image1', 'image2', 'image3', 'image4'], image, even = true, imageWidth;
    for (var i = 0; i < id_images.length; i++) {
        image = document.getElementById(id_images[i]);
        imageWidth = image.clientWidth;
        image.onclick = function (even, imageWidth) {
            return function () {
                for (var j = 0; j < id_images.length; j++) {
                    var imageJ = document.getElementById(id_images[j]);
                    imageJ.style.width = imageWidth + "px";
                }
                if (even) {
                    this.style.width = imageWidth * 4 + "px";
                    even = false;
                } else {
                    //this.style.width = imageWidth + "px";
                    even = true;
                }
            }
        }(even, imageWidth);
    }
};