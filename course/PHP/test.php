<html>
<head>
    <title>Тестируем знания PHP</title>
    <meta charset="utf-8">
    <style>
        html, body {
            height: 100%;
            margin: 15px;
        }

        H2 {
            font-size: 120%;
            font-family: Arial, Helvetica, sans-serif;
            color: gray;
        }
    </style>
</head>
<body>
<form action="action.php" method="post">

    <h3> Вопрос № 1. Какое расширение имеют файлы PHP? </h3>
    <p>
    <ol>
        <input type="radio" name="a" value="1">.php<br>
        <input type="radio" name="a" value="2">.пхп<br>
        <input type="radio" name="a" value="3">.pxp<br>
    </ol>
    </p>

    <h3> Вопрос № 2. Где происходит формирование страницы PHP? </h3>
    <p>
    <ol>
        <input type="radio" name="b" value="1">На стороне клиента<br>
        <input type="radio" name="b" value="2">На стороне сервера<br>
        <input type="radio" name="b" value="3">И там и там<br>
    </ol>
    </p>

    <h3> Вопрос № 3. PHP это компилируемый или интерпретируемый язык? </h3>
    <p>
    <ol>
        <input type="radio" name="c" value="1">Интерпретируемый<br>
        <input type="radio" name="c" value="2">Компилируемый<br>
        <input type="radio" name="c" value="3">Ни то ни то<br>
    </ol>
    </p>

    <h3> Вопрос № 4. Какое животное изображено на логотипе PHP? </h3>
    <p>
    <ol>
        <input type="radio" name="d" value="1">Пингвин<br>
        <input type="radio" name="d" value="2">Слон<br>
        <input type="radio" name="d" value="3">Динозавр T-Rex<br>
    </ol>
    </p>

    <h3> Вопрос № 5. Как сейчас расшифровывается PHP?</h3>
    <p>
    <ol>
        <input type="radio" name="e" value="1">Personal Home Page Tools<br>
        <input type="radio" name="e" value="2">PHP: Hypertext Preprocessor<br>
        <input type="radio" name="e" value="3">Что-то еще<br>
    </ol>
    </p>

    <p><input type="submit"/></p>
</form>
</body>
</html>