#include <iostream>

int main() {

    int length=std::atol(getenv("CONTENT_LENGTH"));
    char *input = new char[length + 1];
    std::cin >> input;

    //example "q1=2&q2=1&q3=1&q4=1&q5=1";
    std::string s(input);
    std::string delimiter = "=";

    size_t pos = 0;
    std::string answer;
    int total = 0;
    while ((pos = s.find(delimiter)) != std::string::npos) {
        answer = s.substr(pos + 1, 1);
        s.erase(0, pos + 2);
        total += std::stoi(answer);
    }

    std::cout << "Content-type: text/html\n";
    std::cout << "Set-Cookie: result=" << total << "\n\n";
    std::cout << "<html>";
    std::cout << "<h2>Ваш результат: </h2>";
    std::cout << "<p> " << total << " баллов. Уровень ";
    if (total <= 10) std::cout << "1 - низкий";
    if (total > 10 && total <= 12) std::cout << "2 - средний";
    if (total > 12) std::cout << "3 - высокий";
    std::cout << "</p>";
    std::cout << "</html>";
}