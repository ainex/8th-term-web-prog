cmake_minimum_required(VERSION 3.13)
project(8th_term)

set(CMAKE_CXX_STANDARD 14)

add_executable(8th_term main.cpp)